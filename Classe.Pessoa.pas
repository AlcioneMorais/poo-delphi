unit Classe.Pessoa;

interface

uses
  System.SysUtils, Classe.Sql;

type
  TPessoa = class
  {  strict private.
    N�o comportilhada com ninguem nem com classes amigas
    Quarto do casal. ninguem fica sabendo
  }
  strict private
    FSegredo : string;

  {  private
     Casa. s� as pessoas da casa sabem s� elas
     tem acesso ao assunto
  }
  private
    FNome: string;
    FDataNasc: String;
    FSexo: string;
    FEtinia: string;
    function getNome: String;
    procedure setNome(Value: String);
    procedure SetDataNasc(const Value: String);
    procedure SetEtnia(const Value: string);
    procedure SetSexo(const Value: string);

  { strict protected.
    s� pessoas da familia tem acesso.
    Amigos n�o tem
  }
  strict protected
    FSoFamilia : String;


  {  protected
   Grupo do whatsapp da familia. As pessoas
    que s�o da familia e n�o moram na casa
    sabem tamb�m ex.. Amigos
  }
  protected
    FWhastsapp : string;

  {  Published.
    Adm do blog por exemplo consegue ver antes de ser publicado
     � possivel acessar em tempo de Designer
     Usado em componentes
  }
  //published
  //  FPost : string;

  {  public
    Postagem no Facebook. Tudo mundo que segue ao acompanha voc�
    vai ter acesso a informa��o
  }
  public
  //Atalho para gerar os metodos das property � ctrl shift C
    property DataNasc: String read FDataNasc write SetDataNasc;
    property Sexo: string read FSexo write SetSexo;
    property Etnia: string read FEtinia write SetEtnia;
    property Nome: string read FNome write setNome;
    function Idade: Integer;
    function Receber(I : Integer): String; overload;
    function Receber(C : Currency): String; overload;
    function Receber(A ,B : Integer): String; overload;
    //virtual -> Pertinite reescrever esta fun��o em outra classe  Ex. Funcionario
    function RetornaNome : String; virtual;
    function RetornaInherited : String; virtual;

    { Metodo abstract n�o � necess�rio ser implementado na classe de cria��o
      e pode ser implementado ou n�o em outra classe}
    function MetodoAbstrato : string; virtual; abstract;
    // Sql : TConSql;  n�o pode
  end;
type
//class amiga n�o � muito aconselhado usar
TTeste = class (TPessoa)
  function Teste : Boolean;
end;

type
 TAmigo = class
   Familia : TPessoa;
   procedure Teste;
 end;

implementation

{ TPessoa }

function TPessoa.Idade: Integer;
begin
  Result := Trunc((now - StrToDate(DataNasc)) / 365.25);
end;

function TPessoa.Receber(C: Currency): String;
begin
  Result := ' Valor Currency: ' + CurrToStr(C);
end;

function TPessoa.Receber(I: Integer): string;
begin
   Result := ' Valor Inteiro: ' + IntToStr(I);
end;

procedure TPessoa.SetDataNasc(const Value: String);
begin
  FDataNasc := Value;
end;

procedure TPessoa.SetEtnia(const Value: string);
begin
  FEtinia := Value;
end;

procedure TPessoa.setNome(Value: String);
begin
  if Value = '' then
    raise Exception.Create('Valor n�o pode ser vazio !');
  FNome := Value;
end;

procedure TPessoa.SetSexo(const Value: string);
begin
  FSexo := Value;
end;

function TPessoa.getNome: string;
begin
  Result := FNome;
end;

function TPessoa.Receber(A, B: Integer): String;
begin
  Result := ' A Soma desses Inteiros �: ' + IntToStr((A + B));
end;

 // antes do inherited
function TPessoa.RetornaNome: String;
begin
  Result :=  'Classe.Pessao';
end;

function TPessoa.RetornaInherited: String;
begin
  Nome :=  'Classe.Pessao';
end;

{ TTeste }
//n�o recomendado
function TTeste.Teste: Boolean;
begin
  Self.FNome := '';
  Self.FWhastsapp := '';
  Self.FSoFamilia := ''; //� da familia
  Result := True;
end;

{ TAmigo }

procedure TAmigo.Teste;
begin
  //N�o tem acesso a variavel FSoFamilia
  Familia.FWhastsapp := '';
end;

end.
