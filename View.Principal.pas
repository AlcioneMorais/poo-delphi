unit View.Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Ac�o: TButton;
    edtNome: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    procedure Ac�oClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses Classe.Cliente, Classe.Pessoa;

{
  4 - principais conceitos

  Abstra��o
  Heran�a
  -> possibilidade de modelar objetos e problemas do mundo real dentro da linguagem.
  -> Capacidade de suporte ao mundo real e a reutiliza��o dos metodos e procedimentos.

  Encapsulamento
  -> Capacidade de criar objetos e procedimentos reutilizaveis, sem se preocupar e regras internas.
    Coes�o -> Comportamentos Unicos, metodos e fun��es que realizam apenas uma unica opera��o
              para facilitarem o seu reaproveitamento.
    Propertys -> encapsular as variav�is da classe
  Polimorfismo
}

{
 Construtores e destruidores j� existem por padr�o na TObject

}
procedure TForm1.Ac�oClick(Sender: TObject);
var
  Pessoa1 : TPessoa;
  Cliente1 : TCliente;
begin
  Pessoa1 := TPessoa.Create;
  Cliente1 := TCliente.Create;
  try
    Pessoa1.Nome := edtNome.Text;
    Pessoa1.DataNasc := '13/09/1985';

    Cliente1.Nome := 'Cassiane Braz';
    Cliente1.DataNasc := '21/10/1994';
    Cliente1.ValorCredito := 12.90;

    //Cliente1.Sql.Gravar;  errado

    ShowMessage('Pessoa  ' + Pessoa1.Nome + '  Idade ' + IntToStr(Pessoa1.Idade));
    ShowMessage('Cliente  ' + Cliente1.Nome + '  Idade ' + IntToStr(Cliente1.Idade) + '  Cr�dito ' + CurrToStr(Cliente1.ValorCredito));
  finally
    Pessoa1.Free;
    Cliente1.Free;
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  Pessoa1 : TPessoa;
  Value : Currency;
begin
  Pessoa1 := TPessoa.Create;
  try
    ShowMessage('Pessoa  ' + Pessoa1.Receber(4));
    Value := 10.4;
    ShowMessage('Pessoa  ' + Pessoa1.Receber(Value));
    ShowMessage('Pessoa  ' + Pessoa1.Receber(3,4));
  finally
    Pessoa1.Free;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  Pessoa1 : TPessoa;
  Cliente1 : TCliente;
begin
  Pessoa1 := TPessoa.Create;
  Cliente1 := TCliente.Create;
  try
     ShowMessage('Pessoa  ' + Pessoa1.RetornaNome);
     ShowMessage('Cliente  ' + Cliente1.RetornaNome);
  finally
    Pessoa1.Free;
    Cliente1.Free;
  end;
end;
procedure TForm1.Button3Click(Sender: TObject);
var
  Cliente1 : TCliente;
begin
  Cliente1 := TCliente.Create;
  try
     ShowMessage('Cliente  ' + Cliente1.MetodoAbstrato);
  finally
    Cliente1.Free;
  end;
end;

procedure TForm1.Button4Click(Sender: TObject);
var
  Cliente1 : TCliente;
begin

  Cliente1 := TCliente.Create;
  try
    ShowMessage('Cliente  ' + Cliente1.RetornaInherited);
  finally
    Cliente1.Free;
  end;
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  Pessoa1 : TPessoa;
  Cliente1, Cliente2, Cliente3 : TCliente;
  begin
  Pessoa1 := TPessoa.Create;
  Pessoa1.Nome := 'Leda Morais';
  Pessoa1.DataNasc := '26/04/1960';
  Pessoa1.Sexo := 'Feminino';

  Cliente1 := TCliente.Create;
  Cliente2 := TCliente.Create('Alcione Morais');
  Cliente3 := TCliente.Create(Pessoa1);
  try
    ShowMessage('Cliente1  ' + Cliente1.Nome);
    ShowMessage('Cliente2  ' + Cliente2.Nome);
    ShowMessage('Cliente3  ' + Cliente3.Nome + ' - '  + IntToStr(Cliente3.Idade) + ' - ' + Cliente3.Sexo);
  finally
    Cliente1.Free;
    Cliente2.Free;
    Cliente3.Free;
    Pessoa1.Free;
  end;
end;

procedure TForm1.Button6Click(Sender: TObject);
var
  Pessoa1 : TPessoa;
  Cliente1 : TCliente;
  begin
  Pessoa1 := TPessoa.Create;
  Cliente1 := TCliente.Create;
  try
    ShowMessage('Cliente1  ' + Cliente1.Endereco.Logradouro);
    finally
    Pessoa1.Free;
    Cliente1.Destroy;
  end;
end;
procedure TForm1.Button7Click(Sender: TObject);
var
  Cliente1 : TCliente;
begin

  Cliente1 := TCliente.Create;
  try
    ShowMessage('Cliente  ' + Cliente1.RetornaInherited);
  finally
    Cliente1.Free;
  end;
end;

end.
