unit Classe.Pessoa;

interface

type
  TPessoa = class
  private
    FSalario: Currency;
    FNome: string;
    procedure SetNome(const Value: string);
    procedure SetSalario(const Value: Currency);
  published
    property Nome : string read FNome write SetNome;
    property Salario : Currency read FSalario write SetSalario;
    function CalcularSalario : Currency; virtual;
  end;

implementation

uses
  System.SysUtils;

{ TPessoa }

function TPessoa.CalcularSalario: Currency;
begin
  Result := Salario;
end;

procedure TPessoa.SetNome(const Value: string);
begin
  if Value = '' then
    raise Exception.Create('Nome n�o pode ser vazio');
  FNome := Value;
end;

procedure TPessoa.SetSalario(const Value: Currency);
begin
  if Value <= 0 then
    raise Exception.Create('Sal�rio n�o pode ser menor ou igual a zero');
  FSalario := Value;
end;

end.
