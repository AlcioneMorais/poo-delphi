object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'View Principal'
  ClientHeight = 325
  ClientWidth = 256
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 31
    Top = 36
    Width = 43
    Height = 13
    Caption = 'Fun'#231#227'o.:'
  end
  object Label2: TLabel
    Left = 32
    Top = 69
    Width = 35
    Height = 13
    Caption = 'Nome.:'
  end
  object Label3: TLabel
    Left = 34
    Top = 101
    Width = 40
    Height = 13
    Caption = 'Sal'#225'rio.:'
  end
  object Label4: TLabel
    Left = 32
    Top = 134
    Width = 37
    Height = 13
    Caption = 'B'#244'nus.:'
  end
  object cbbFuncao: TComboBox
    Left = 80
    Top = 33
    Width = 145
    Height = 21
    ItemIndex = 0
    TabOrder = 0
    Text = 'Administrativo'
    Items.Strings = (
      'Administrativo'
      'Vendedor'
      'Funcion'#225'rio')
  end
  object edtNome: TEdit
    Left = 80
    Top = 66
    Width = 145
    Height = 21
    TabOrder = 1
  end
  object edtSalario: TEdit
    Left = 80
    Top = 98
    Width = 145
    Height = 21
    TabOrder = 2
  end
  object edtBonus: TEdit
    Left = 80
    Top = 131
    Width = 145
    Height = 21
    TabOrder = 3
  end
  object Button1: TButton
    Left = 31
    Top = 168
    Width = 82
    Height = 25
    Caption = 'Cadastrar.:'
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 126
    Top = 168
    Width = 43
    Height = 25
    Caption = '<<'
    TabOrder = 5
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 184
    Top = 168
    Width = 41
    Height = 25
    Caption = '>>'
    TabOrder = 6
    OnClick = Button3Click
  end
  object Memo1: TMemo
    Left = 31
    Top = 228
    Width = 194
    Height = 89
    TabOrder = 7
  end
  object Button4: TButton
    Left = 31
    Top = 197
    Width = 194
    Height = 25
    Caption = 'Calcular Sal'#225'rios.:'
    TabOrder = 8
    OnClick = Button4Click
  end
end
