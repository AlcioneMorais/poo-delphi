unit View.Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Classe.Administrativa,
  Classe.Pessoa, Classe.Vendedor;

type
  TEnumFuncao = (tpAdministrativo, tpVendedor, tpFuncionario);

type
  TForm2 = class(TForm)
    Label1: TLabel;
    cbbFuncao: TComboBox;
    Label2: TLabel;
    edtNome: TEdit;
    edtSalario: TEdit;
    Label3: TLabel;
    edtBonus: TEdit;
    Label4: TLabel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Memo1: TMemo;
    Button4: TButton;
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure fnc_ExibirDados(Pos: Integer);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    procedure fnc_IncluirAdministrativo;
    procedure fnc_IncluirVendedor;
    procedure fnc_IncluirFuncionario;
    { Private declarations }
  public
    { Public declarations }
    Funcionario: Array [1 .. 100] of TPessoa;
    QtdFunc: Integer;
    RegAtual : Integer;
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
begin
  try
    case TEnumFuncao(cbbFuncao.ItemIndex) of
    tpAdministrativo:
      begin
        fnc_IncluirAdministrativo;
      end;
    tpVendedor:
      begin
        fnc_IncluirVendedor;
      end;
    tpFuncionario:
      begin
        fnc_IncluirFuncionario;
      end;
  end;
  Inc(QtdFunc);
  except
    raise Exception.Create('Erro ao cadastrar Funcion�rio');
  end;

end;

procedure TForm2.fnc_IncluirAdministrativo;
begin
  // ver classe helper
  Funcionario[QtdFunc] := TAdministrativo.Create;
  TAdministrativo(Funcionario[QtdFunc]).Bonus := StrToCurr(edtBonus.Text);
  TAdministrativo(Funcionario[QtdFunc]).Nome := edtNome.Text;
  TAdministrativo(Funcionario[QtdFunc]).Salario := StrToCurr(edtSalario.Text);
end;

procedure TForm2.fnc_IncluirVendedor;
begin
  Funcionario[QtdFunc] := TVendedor.Create;
  TVendedor(Funcionario[QtdFunc]).Comissao := StrToCurr(edtBonus.Text);
  TVendedor(Funcionario[QtdFunc]).Nome := edtNome.Text;
  TVendedor(Funcionario[QtdFunc]).Salario := StrToCurr(edtSalario.Text);
end;

procedure TForm2.fnc_IncluirFuncionario;
begin
  Funcionario[QtdFunc] := TPessoa.Create;
  Funcionario[QtdFunc].Nome := edtNome.Text;
  Funcionario[QtdFunc].Salario := StrToCurr(edtSalario.Text);
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  if RegAtual <= 0 then
    exit ;

  RegAtual := RegAtual - 1;
  fnc_ExibirDados(RegAtual);
end;

procedure TForm2.Button3Click(Sender: TObject);
begin
  if RegAtual >= QtdFunc - 1 then
    exit ;

  RegAtual := RegAtual + 1;
  fnc_ExibirDados(RegAtual);
end;

procedure TForm2.Button4Click(Sender: TObject);
var
  Total: Currency;
  I: Integer;
begin
  Total := 0;
  for I := 0 to QtdFunc - 1 do
  begin
    Total := Total + Funcionario[I].CalcularSalario;
  end;

  Memo1.Lines.Add('Sua Folha Salarial � de: R$ ' + CurrToStr(Total));

end;

procedure TForm2.fnc_ExibirDados(Pos: Integer);
begin
  edtNome.Text := Funcionario[RegAtual].Nome;
  edtSalario.Text := CurrToStr(Funcionario[RegAtual].Salario);

  if (Funcionario[RegAtual].ClassName = 'TAdministrativo') then
  begin
    edtBonus.Enabled := True;
    edtBonus.Text := CurrToStr(TAdministrativo(Funcionario[RegAtual]).Bonus);
    cbbFuncao.ItemIndex := Integer(tpAdministrativo);
  end;

  if (Funcionario[RegAtual].ClassName = 'TVendedor') then
  begin
    edtBonus.Enabled := True;
    edtBonus.Text := CurrToStr(TVendedor(Funcionario[RegAtual]).Comissao);
    cbbFuncao.ItemIndex := Integer(tpVendedor);
  end;

  if (Funcionario[RegAtual].ClassName = 'TFuncionario') then
  begin
    edtBonus.Enabled := False;
    edtBonus.Text := '';
    cbbFuncao.ItemIndex := Integer(tpFuncionario);

  end;

end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  QtdFunc := 0;
  RegAtual := 0;
end;

end.
