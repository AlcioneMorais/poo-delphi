unit Classe.Cliente;

interface

uses Classe.Pessoa, Classe.Endereco;

type
  TCliente = class(TPessoa)   // Heran�a
  private
    FValorCredito : Currency;
    FEndereco: TEndereco;
    Procedure SetValorCredito(const Value: Currency);
    procedure SetEndereco(const Value: TEndereco);
  public
    constructor Create; overload;
    constructor Create(Value : String); overload;
    constructor Create(Value : TPessoa); overload;
    {
      O reintroduce � necess�rio reimprementar chamar o Destroy em vez de Free
      ele tambem oculta as mensagem no compilador

      destructor Destroy; reintroduce;

      J� o override recria e basta apenas usar o Free que chama
      o destroy automaticamente
      destructor Destroy; override;
    }
    destructor Destroy; override;

    function RetornaNome : string; override;
    function RetornaInherited : string; override;
    function MetodoAbstrato : string; override;

    property ValorCredito : Currency read FValorCredito write SetValorCredito;
    property Endereco : TEndereco read FEndereco write SetEndereco;
  end;
implementation

{ TCliente }

constructor TCliente.Create;
begin
  FEndereco := TEndereco.Create;
  Nome := 'Novo Cliente';
end;

constructor TCliente.Create(Value: String);
begin
  Nome := Value;
end;
// tipo da classe
constructor TCliente.Create(Value: TPessoa);
begin
  Nome := Value.Nome;
  DataNasc := Value.DataNasc;
  Sexo := Value.Sexo;
end;

destructor TCliente.Destroy;
begin
  //limpar dependencia
  FEndereco.Free;
end;

function TCliente.MetodoAbstrato: string;
begin
  Result := ' Metodo Astrato ';
end;

function TCliente.RetornaNome: string;
begin
  Result := ' Classe.Cliente ';
end;

function TCliente.RetornaInherited: string;
begin
  // inherited = herdado
  inherited;
  Self.FWhastsapp := '';
  Result := ' Eu sou filha de ' + Nome;
end;


procedure TCliente.SetEndereco(const Value: TEndereco);
begin
  FEndereco := Value;
end;

procedure TCliente.SetValorCredito(const Value: Currency);
begin
  FValorCredito := Value;
end;

end.
