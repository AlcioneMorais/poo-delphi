﻿object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Tela In'#237'cial'
  ClientHeight = 354
  ClientWidth = 343
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Acão: TButton
    Left = 48
    Top = 88
    Width = 185
    Height = 25
    Caption = 'Ac'#227'o'
    TabOrder = 0
    OnClick = AcãoClick
  end
  object edtNome: TEdit
    Left = 48
    Top = 40
    Width = 185
    Height = 21
    TabOrder = 1
  end
  object Button1: TButton
    Left = 48
    Top = 119
    Width = 185
    Height = 25
    Caption = ' Polimorfismo (Overload)'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 48
    Top = 150
    Width = 185
    Height = 25
    Caption = 'Polimorfismo (Virtual e Override)'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 48
    Top = 181
    Width = 185
    Height = 25
    Caption = 'Polimorfismo (Abstract)'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 48
    Top = 212
    Width = 185
    Height = 25
    Caption = 'Polimorfismo e Heran'#231'a (inherited)'
    TabOrder = 5
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 48
    Top = 243
    Width = 185
    Height = 25
    Caption = 'Construtores'
    TabOrder = 6
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 48
    Top = 274
    Width = 185
    Height = 25
    Caption = 'Destruidores'
    TabOrder = 7
    OnClick = Button6Click
  end
end
